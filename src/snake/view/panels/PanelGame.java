package snake.view.panels;

import snake.model.GameModel;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.model.subgamemodel.GameConstants;
import snake.model.subgamemodel.Snake;
import snake.model.subgamemodel.WallLine;
import snake.model.subgamemodel.solids.Fruits;
import snake.model.subgamemodel.solids.Tail;
import snake.model.subgamemodel.solids.Wall;
import snake.view.ViewConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Victor on 4/27/2015.
 */
public class PanelGame extends JPanel implements GameConstants, ViewConstants, GlobalConstants {

    private static final byte HEAD_DIAMETER = HEAD_RAY * 2;
    private static final byte FRUITS_DIAMETER = FRUITS_RAY * 2;
    private static final byte ECART_EYES = HEAD_DIAMETER / 3;
    private static final byte DIAMETER_EYES = ECART_EYES / 2;
    private static final byte RAY_EYES = DIAMETER_EYES / 2;
    private static final double ANGLE_EYES = Math.toRadians(30);
    private static ArrayList<BufferedImage> fruits = new ArrayList<BufferedImage>(4);


    public short x = 0;
    public short y = 0;
    public byte calories = 0;
    public Graphics2D g2;
    private Font mainFont;
    private Font playFontSize;
    private Font subMenuSize;
    private GameModel model;
    private BufferedImage pnghead;
    private AffineTransform orig;

    public PanelGame(MainModel model) {
        this.model = model.getGame();
        try {
            mainFont = Font.createFont(Font.TRUETYPE_FONT, new File("assets/font/OpenSans-Light.ttf"));
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        initFruits();

    }

    public void updateModel(MainModel model) {
        this.model = model.getGame();
    }

    @Override
    public void paintComponent(Graphics g) {

        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        playFontSize = mainFont.deriveFont(40f);
        subMenuSize = mainFont.deriveFont(30f);

        orig = g2.getTransform();

        g2.setColor(new Color(50, 136, 152));
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        drawFruit();
        drawTail();
        drawHead();
        drawWall();


    }
    public void isDead(){
        if (model.isEnd()){

        }
    }
    public void drawFruit() {
        for (Fruits frut : model.fruits) {
            if (frut.isEatable()){
               if (frut.calories<=8){
                   g2.drawImage(fruits.get(0),frut.getX()-15,frut.getY()-15,this);
               }
                else if (frut.calories<=12){
                    g2.drawImage(fruits.get(1),frut.getX()-15,frut.getY()-15,this);
                }
               else if (frut.calories<=16){
                   g2.drawImage(fruits.get(2),frut.getX()-15,frut.getY()-15,this);
               }
               else if (frut.calories<=21){
                   g2.drawImage(fruits.get(3),frut.getX()-15,frut.getY()-15,this);
               }
            }

        }
        //Todo : Affichage du fruit à n calories centré en x et y.
    }

    public void drawTail() {
        int playerNumber = 0;
        for (Snake player : model.players) {
            for (Tail frag : player.tails) {
                g2.setColor(TAIL_COLOR[playerNumber]);
                g2.fillOval(frag.getX() - frag.getRay(), frag.getY() - frag.getRay(), frag.getDiameter(), frag.getDiameter());
                g2.setColor(HEAD_COLOR[playerNumber]);
                g2.drawOval(frag.getX() - frag.getRay(), frag.getY() - frag.getRay(), frag.getDiameter(), frag.getDiameter());

            }
            playerNumber++;
        }
    }

    public void drawHead() {
        int playerNumber = 0;
        for (Snake player : model.players) {

            int x = player.head.getX() - HEAD_RAY;
            int y = player.head.getY() - HEAD_RAY;
            double a = player.head.getAngle();

            g2.setColor(HEAD_COLOR[playerNumber]);
            g2.fillOval(x, y, HEAD_DIAMETER, HEAD_DIAMETER);

            g2.setColor(Color.black);
            g2.drawOval(x, y, HEAD_DIAMETER, HEAD_DIAMETER);
            g2.fillOval((int) (player.head.getX() + Math.cos(a + ANGLE_EYES) * ECART_EYES) - RAY_EYES,
                    (int) (player.head.getY() + Math.sin(a + ANGLE_EYES) * ECART_EYES) - RAY_EYES, DIAMETER_EYES, DIAMETER_EYES);
            g2.fillOval((int) (player.head.getX() + Math.cos(a - ANGLE_EYES) * ECART_EYES) - RAY_EYES,
                    (int) (player.head.getY() + Math.sin(a - ANGLE_EYES) * ECART_EYES) - RAY_EYES, DIAMETER_EYES, DIAMETER_EYES);
            playerNumber++;
        }
    }

    public void drawWall() {
        for (WallLine wallLine : model.wallLines)
            for (Wall wall : wallLine.getWalls()) {
                g2.setColor(new Color(255, 245, 250));
                g2.fillOval(wall.getX() - wall.getRay(), wall.getY() - wall.getRay(), wall.getRay() * 2, wall.getRay() * 2);
            }
    }

    public void setKeyControler(KeyListener keyListener) {
        addKeyListener(keyListener);
    }
    public void initFruits(){
        try {
            fruits.add(ImageIO.read(new File("img/grapefruit.png")));
            fruits.add(ImageIO.read(new File("img/banana.png")));
            fruits.add(ImageIO.read(new File("img/lollipop.png")));
            fruits.add(ImageIO.read(new File("img/burger.png")));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
