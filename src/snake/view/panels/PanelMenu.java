package snake.view.panels;

import javafx.scene.paint.*;
import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.*;

/**
 * Created by Mahel on 26/04/2015.
 */
//Class pour la g�n�ration du menu principal

public class PanelMenu extends JPanel implements GlobalConstants, ViewConstants {


    public JButton playButton;
    public JButton highscoreButton;
    public JButton exitButton;
    public MainModel model;
    public Graphics2D g2;
    public int step = 0;
    private Font mainFont;
    private Font playFontSize;
    private Font subMenuSize;
    private AffineTransform orig;
    private Color color = GAME_COLOR;


    public PanelMenu(MainModel model) {
        this.model = model;
        playButton = new JButton();
        playButton.setBounds(WINDOW_WIDTH / 2 + 170, WINDOW_HEIGHT / 2 - 20, 150, 50);
        playButton.setContentAreaFilled(false);
        playButton.setFocusPainted(false);
        playButton.setMargin(null);
        playButton.setBorder(BorderFactory.createEmptyBorder());
        this.add(playButton);

        try {
            mainFont = Font.createFont(Font.TRUETYPE_FONT, new File("assets/font/OpenSans-Light.ttf"));
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

        TimerTask task = new TimerTask() {
            int r = GAME_COLOR.getRed();
            int g = GAME_COLOR.getGreen();
            int b = GAME_COLOR.getBlue();
            boolean revert = false;

            @Override
            public void run() {
                if (r < 255 && !revert) {
                    r++;
                } else if (r > 0 && revert) {
                    r--;
                }
                if (g > 50 && !revert) {
                    g--;
                } else if (g < 255 && revert) {
                    g++;
                }
                if (b < 255 && !revert) {
                    b++;
                } else if (b > 0 && revert) {
                    b--;
                }
                if (r >= 255) {
                    revert = true;
                }
                if (r <= 0) {
                    revert = false;
                }
                color = new Color(r, g, b);
                repaint();
            }

        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 100);
    }

    private void drawSubMenu(String s, int XPos, int YPos) {
        FontMetrics fm = g2.getFontMetrics();
        java.awt.geom.Rectangle2D rect = fm.getStringBounds(s, g2);
        int textHeight = (int) (rect.getHeight());
        int x = XPos;
        int y = YPos - textHeight / 4;

        g2.drawString(s, x, y + ((fm.getDescent() + fm.getAscent()) / 2));
    }


    @Override
    public void paintComponent(Graphics g) {
        int mainCircleSize = 300;

        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        playFontSize = mainFont.deriveFont(40f);
        subMenuSize = mainFont.deriveFont(30f);

        orig = g2.getTransform();

        g2.setColor(color);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        g2.setColor(Color.black);
        g2.drawOval(WINDOW_WIDTH / 2 - mainCircleSize / 2, WINDOW_HEIGHT / 2 - mainCircleSize / 2, mainCircleSize, mainCircleSize);

        drawStringPlay();
        drawStringHighscore();
        drawStringExit();
    }

    public void drawStringPlay() {
        g2.rotate(Math.toRadians(0), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        g2.setFont(playFontSize);
        drawSubMenu("Play", WINDOW_WIDTH / 2 + 170 + step, WINDOW_HEIGHT / 2);
        g2.setTransform(orig); //R�nitialise la rotation du calque par d�faut*/
    }

    public void drawStringHighscore() {
        g2.setFont(subMenuSize);
        g2.rotate(Math.toRadians(-15), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        drawSubMenu("Highscores", WINDOW_WIDTH / 2 + 170, WINDOW_HEIGHT / 2);
        g2.setTransform(orig);
    }

    public void drawStringExit() {
        g2.setFont(subMenuSize);
        g2.rotate(Math.toRadians(15), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
        drawSubMenu("Exit", WINDOW_WIDTH / 2 + 170, WINDOW_HEIGHT / 2);
        g2.setTransform(orig);
    }


    public void setMenuControler(MouseMotionListener listener) {
        addMouseMotionListener(listener);
    }

    public void setMenuMouseControler(MouseListener listener) {
        playButton.addMouseListener(listener);
    }

    public void setActionControler(ActionListener listener) {
        playButton.addActionListener(listener);
    }
}