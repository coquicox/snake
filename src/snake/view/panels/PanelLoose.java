package snake.view.panels;

import snake.model.GlobalConstants;
import snake.model.MainModel;
import snake.view.ViewConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

/**
 * Created by Mahel on 24/05/2015.
 */
public class PanelLoose extends JPanel implements GlobalConstants, ViewConstants {

    public JButton newgame;
    public JButton exit;
    public MainModel model;
    public Graphics2D g2;
    private Font mainFont;
    private Font overFontSize;
    private Font buttonFontSize;
    private AffineTransform orig;
    public Color colornewgame=new Color(0,0,0);
    public Color colorexit=new Color(0,0,0);


    public PanelLoose(MainModel model) {
        this.model = model;
        newgame = new JButton();
        exit = new JButton();
        newgame.setBounds(WINDOW_WIDTH / 2-WINDOW_WIDTH / 4+25, WINDOW_HEIGHT / 2, WINDOW_WIDTH / 4-50, 100);
        exit.setBounds(WINDOW_WIDTH / 2+25, WINDOW_HEIGHT / 2, WINDOW_WIDTH / 4-50, 100);
        newgame.setContentAreaFilled(false);
        /*newgame.setFocusPainted(false);
        newgame.setMargin(null);
        newgame.setBorder(BorderFactory.createEmptyBorder());*/
        exit.setContentAreaFilled(false);
        /*exit.setFocusPainted(false);
        exit.setMargin(null);
        exit.setBorder(BorderFactory.createEmptyBorder());*/
        this.add(newgame);
        this.add(exit);

        try {
            mainFont = Font.createFont(Font.TRUETYPE_FONT, new File("assets/font/OpenSans-Light.ttf"));
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setLayout(null);
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));

    }

    @Override
    public void paintComponent(Graphics g) {
        int mainCircleSize = 300;

        g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        overFontSize = mainFont.deriveFont(100f);
        buttonFontSize = mainFont.deriveFont(40f);
        orig = g2.getTransform();

        g2.setColor(GAME_COLOR);
        g2.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        g2.setColor(Color.black);
        g2.setFont(overFontSize);
        g2.drawString("Game Over", WINDOW_WIDTH / 2 - 250, WINDOW_HEIGHT / 2 - 100);
        g2.setFont(buttonFontSize);
        g2.setColor(colornewgame);
        g2.drawString("New Game", WINDOW_WIDTH / 2 - WINDOW_WIDTH / 4 + 25, WINDOW_HEIGHT / 2 + 55);
        g2.setColor(colorexit);
        g2.drawString("Menu", WINDOW_WIDTH / 2 + 70, WINDOW_HEIGHT / 2 + 55);

    }
    public void setMouseListener(MouseListener listener){
        newgame.addMouseListener(listener);
        exit.addMouseListener(listener);
    }
    public void setActionListener(ActionListener listener){
        newgame.addActionListener(listener);
        exit.addActionListener(listener);
    }

}
