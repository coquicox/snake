package snake.view;

import snake.model.GameModel;
import snake.model.MainModel;
import snake.model.subgamemodel.GameConstants;
import snake.view.panels.PanelGame;
import snake.view.panels.PanelLoose;
import snake.view.panels.PanelMenu;
import snake.view.panels.PanelPause;

import javax.swing.*;

/**
 * Created by gcaron on 13/05/15.
 */
public class MainWindow extends JFrame implements GameConstants {
    private PanelGame game;
    private PanelMenu menu;
    private PanelPause pause;
    private PanelLoose loose;

    public MainWindow(MainModel model) {
        this.setTitle("Snake Beta");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.game = new PanelGame(model);
        this.menu = new PanelMenu(model);
        this.loose = new PanelLoose(model);
        this.pause = new PanelPause();

        setContentPane(menu);
        this.pack();
        this.setVisible(true);
    }

    public PanelGame getGamePanel() {return game;}

    public PanelMenu getMenuPanel() {
        return menu;
    }

    public PanelPause getPausePanel() {
        return pause;
    }
    public PanelLoose getLoosePanel() {
        return loose;
    }

}
