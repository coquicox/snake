package snake.view;

import java.awt.*;

/**
 * Created by vfranco3 on 22/05/15.
 */
public interface ViewConstants {
    public final static Color GAME_COLOR = new Color(50, 136, 152);
    public final static Color[] HEAD_COLOR = {new Color(0, 153, 76), new Color(204, 0, 204)};
    public final static Color[] TAIL_COLOR = {new Color(0, 204, 102), new Color(255, 0, 255)};


}
