package snake.model.subgamemodel.solids;

import snake.lib.GameTickCounter;
import snake.model.GlobalConstants;

/**
 * Created by gcaron on 13/04/15.
 */
public class Tail extends SolidCircle implements GlobalConstants {

    private GameTickCounter life;
    private short diameter;

    public Tail(short x, short y, int lifeDuration) {
        super(x, y, TAIL_RAY);
        this.diameter = TAIL_RAY * 2;
        this.life = new GameTickCounter(lifeDuration);
    }

    public void update(int decrease) {
        life.update(decrease);
        if (life.get() < TAIL_RAY) {
            diameter -= decrease;
            ray = (short) (diameter / 2);
        }
    }

    public short getDiameter() {
        return diameter;
    }

    public boolean isVisible() {
        return ray > 0;
    }

    public void addLife(int life) {
        this.life.add(life);
    }
}
