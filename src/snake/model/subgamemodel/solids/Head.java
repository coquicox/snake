package snake.model.subgamemodel.solids;

import snake.lib.GameTickCounter;
import snake.model.GlobalConstants;
import snake.model.subgamemodel.GameConstants;
import snake.model.subgamemodel.Snake;

import static java.lang.Math.*;

/**
 * Created by gcaron on 13/04/15.
 */
public class Head extends SolidCircle implements GlobalConstants, GameConstants {
    private GameTickCounter put;
    private double angle;
    private Snake body;
    private double innerX, innerY;

    public Head(short x, short y, double angle, Snake body) {
        super(x, y, HEAD_RAY);
        innerX = x;
        innerY = y;
        this.body = body;
        this.angle = angle;
        this.put = new GameTickCounter();
    }

    public boolean collideWith(Tail other) {
        int difX = (int) (this.x + cos(angle) * ray) - other.x;
        int difY = (int) (this.y + sin(angle) * ray) - other.y;
        return (difX * difX + difY * difY) < (other.ray * other.ray);
    }

    public void move(boolean isAccelerate) {
        int speed = isAccelerate ? SPEED * 2 : SPEED;
        this.innerX += cos(angle) * speed;
        this.innerY += sin(angle) * speed;
        x = (short) innerX;
        y = (short) innerY;
    }

    public void rotate(char side) {
        switch (side) {
            case 'r':
                angle += ROTATION_SPEED;
                if (angle > PI) angle = -PI;
                break;
            case 'l':
                angle -= ROTATION_SPEED;
                if (angle < -PI) angle = PI;
                break;
        }
    }

    public void grow() {
        if (!put.isUp()) {
            this.body.tails.add(new Tail(x, y, this.body.snakeLong));
            put.set(TICK_TO_GROW);
        }
    }

    public void update(boolean isAccelerate) {
        grow();
        if (isAccelerate)
            grow();
        move(isAccelerate);
    }

    public double getAngle() {
        return angle;
    }

    public void dispose() {
        put = null;
        body = null;
    }
}
