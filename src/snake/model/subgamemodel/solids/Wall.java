package snake.model.subgamemodel.solids;

/**
 * Created by gcaron on 13/04/15.
 * Class de mur du Snake.
 */
public class Wall extends SolidCircle {

    public Wall(short x, short y, short ray) {
        super(x, y, ray);
    }

}
