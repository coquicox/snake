package snake.model.subgamemodel.solids;

import snake.lib.GameTickCounter;
import snake.model.GlobalConstants;

/**
 * Created by gcaron on 13/04/15.
 */
public class Fruits extends SolidCircle implements GlobalConstants {

    public byte calories;
    private GameTickCounter life;

    public Fruits(short x, short y, byte calories, short lifeDuration) {
        super(x, y, FRUITS_RAY);
        this.calories = calories;
        this.life = new GameTickCounter(lifeDuration);
    }

    public void update() {
        this.life.update();
    }

    public boolean isEatable() {
        return this.life.isActive();
    }

    public void eat() {
        life.set();
    }

    /*public void draw(PanelGame panel) {
        if (isEatable())
            panel.drawFruit(this.x,this.y,this.calories);
    }*/
}
