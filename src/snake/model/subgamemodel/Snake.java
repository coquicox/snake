package snake.model.subgamemodel;

import snake.model.subgamemodel.solids.Fruits;
import snake.model.subgamemodel.solids.Head;
import snake.model.subgamemodel.solids.Tail;
import snake.model.subgamemodel.solids.Wall;

import java.util.ArrayList;

/**
 * Created by gcaron on 13/04/15.
 */
public class Snake implements GameConstants {

    public ArrayList<Tail> tails;
    public Head head;
    public int selfScore;
    public String name;
    public boolean isAccelerate;
    public char rotate;
    public int snakeLong;

    public Snake(short x, short y) {
        this.tails = new ArrayList<Tail>(10);
        this.head = new Head(x, y, (short) 0, this);
        this.isAccelerate = false;
        this.snakeLong = TAIL_LIFE_DURATION_INIT;
        this.rotate = 'n';
    }

    public int getScore() {
        return selfScore;
    }

    public void setScore(int score) {
        this.selfScore = score;
    }

    public void update() {
        head.rotate(rotate);
        head.update(isAccelerate);
        ArrayList<Tail> rmTails = new ArrayList<Tail>(2);
        for (Tail frag : tails) {
            frag.update(isAccelerate ? 2 : 1);
            if (!frag.isVisible())
                rmTails.add(frag);
        }
        for (Tail frag : rmTails)
            tails.remove(frag);
        this.selfScore += tails.size() / SCORE_SIZE_DIVIDEND;
    }

    public void eat(Fruits fruit) {
        if (fruit.isEatable()) {
            snakeLong += fruit.calories;
            this.selfScore += fruit.calories * SCORE_FRUITS_FACTOR;
            fruit.eat();
            for (Tail frag : tails)
                frag.addLife(fruit.calories);
        }
    }

    public boolean collideWith(Fruits fruits) {
        if (this.head.collideWith(fruits))
            eat(fruits);
        return false;
    }

    public boolean collideWith(Snake snake) {
        if ((!this.equals(snake)) && this.head.collideWith(snake.head))
            return true;
        for (Tail frag : snake.tails)
            if (frag.isVisible() && this.head.collideWith(frag))
                return true;
        return false;
    }

    public boolean collideWith(Wall wall) {
        return this.head.collideWith(wall);
    }


    public void rotate(char side) {
        this.rotate = side;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void accelerate(boolean isAccelerate) {
        this.isAccelerate = isAccelerate;
    }

    public ArrayList<Tail> getTails() {
        return tails;
    }

    public void dispose() {
        name = null;
        head.dispose();
        head = null;
        tails.clear();
    }

    @Override
    public String toString() {
        return name;
    }
}