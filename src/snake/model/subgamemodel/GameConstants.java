package snake.model.subgamemodel;

import snake.model.GlobalConstants;

/**
 * Created by gcaron on 13/04/15.
 */
public interface GameConstants {
    public static final byte SNAKE_GENERATION_MARGIN = 10;
    public static final byte SNAKE_GENERATION_MARGIN_FOR_OTHER = GlobalConstants.HEAD_RAY + SNAKE_GENERATION_MARGIN;

    public static final byte FRUITS_GENERATION_MARGIN_FOR_FRUITS = 34;
    public static final byte FRUITS_GENERATION_MARGIN_FOR_WALL = 32;
    public static final byte FRUITS_GENERATION_MARGIN_FOR_HEAD = 35;

    public static final byte TAIL_LIFE_DURATION_INIT = 50;

    public static final byte SCORE_SIZE_DIVIDEND = 5;
    public static final byte SCORE_FRUITS_FACTOR = 3;

    public static final byte SPEED = 2;
    public static final byte TICK_TO_GROW = 7;

    public static final byte N_WALL_LINE_MIN = 2;
    public static final byte N_WALL_LINE_MAX = 4;
    public static final short X_WALL_LINE_AREA_MARGIN = (short) (GlobalConstants.WINDOW_WIDTH / 10);
    public static final short Y_WALL_LINE_AREA_MARGIN = (short) (GlobalConstants.WINDOW_HEIGHT / 10);
    public static final short AMPLITUDE_WALL_LINE_MIN = 120;
    public static final short AMPLITUDE_WALL_LINE_MAX = 160;
    public static final float COEFFICIENT_N_WALL = 0.18f;
    public static final byte RAY_WALL_MIN = 10;
    public static final byte RAY_WALL_MAX = 11;

    public static final double ROTATION_SPEED = Math.PI / 48;

    public static final byte MIN_CALORIES = 4;
    public static final byte MAX_CALORIES = 21;
    public static final float DURATION_FACTOR = 1.9f;
    public static final short DURATION_MAX = 500;
}
