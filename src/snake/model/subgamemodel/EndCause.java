package snake.model.subgamemodel;

/**
 * Created by chlorodatafile on 16/05/15.
 */
public enum EndCause {
    SELF_EATING("s'est mangé la queue, il a perdu."),
    SMASH_A_WALL("s'est pris un mur, il a perdu."),
    EAT_THE_ENEMIES("a mangé son adversaire, il a perdu.");

    private static final String textPart1 = "Le serpent";
    private final String textPerso;

    private EndCause(String textPerso) {
        this.textPerso = textPerso;
    }

    public String makeEndSentences(Snake looser) {
        return textPart1 + ' ' + looser + ' ' + textPerso;
    }
}
