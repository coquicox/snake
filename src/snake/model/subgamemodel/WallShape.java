package snake.model.subgamemodel;

import java.util.Random;

/**
 * Created by chlorodatafile on 24/05/15.
 */
public enum WallShape implements GameConstants {
    SINa, COSa, SINb, COSb, EXP, SQR, LOG, POW;

    //ToDo : Corriger le bug de mur long tout le long de la scène de jeu

    private static final double PId2 = Math.PI / 2;

    public static WallShape getRandomShape(Random random) {
        int ordinal = random.nextInt(values().length);
        for (WallShape shape : values())
            if (shape.ordinal() == ordinal)
                return shape;
        return POW;
    }

    public int getComputedValue(float in, int amplitude) {
        switch (this) {
            case SINa:
                return (int) (Math.sin(in * PId2) * amplitude);
            case COSa:
                return (int) (Math.cos(in * PId2) * amplitude);
            case SINb:
                return (int) (Math.sin(in * PId2 + PId2) * amplitude);
            case COSb:
                return (int) (Math.cos(in * PId2 + PId2) * amplitude);
            case EXP:
                return (int) (Math.exp(in) * amplitude);
            case SQR:
                return (int) (Math.sqrt(in) * amplitude);
            case LOG:
                return (int) (Math.log(in) * amplitude);
            case POW:
                return (int) (Math.pow(in, 2) * amplitude);
            default:
                return (int) (in * amplitude);
        }
    }
}
