package snake.model.subgamemodel;

import snake.model.GlobalConstants;
import snake.model.subgamemodel.solids.Wall;

import java.util.Random;

/**
 * Created by chlorodatafile on 24/05/15.
 */
public class WallLine implements GameConstants,GlobalConstants {
    private final Wall[] walls;

    public WallLine(Random random) {
        final int AMPLITUDE = AMPLITUDE_WALL_LINE_MIN + random.nextInt(AMPLITUDE_WALL_LINE_MAX - AMPLITUDE_WALL_LINE_MIN);
        walls = new Wall[(int) (AMPLITUDE * COEFFICIENT_N_WALL)];
        final int origX = X_WALL_LINE_AREA_MARGIN + random.nextInt(WINDOW_WIDTH - X_WALL_LINE_AREA_MARGIN);
        final int origY = Y_WALL_LINE_AREA_MARGIN + random.nextInt(WINDOW_HEIGHT - Y_WALL_LINE_AREA_MARGIN);
        final int dirX, dirY;
        final WallShape shape = WallShape.getRandomShape(random);
        final boolean isXShape = random.nextBoolean();
        if (isXShape) {
            dirY = random.nextInt(5) - 2;
            dirX = (int) ((random.nextInt(1) - 0.5) * (dirY == 0 ? 4 : 2));
        } else {
            dirX = random.nextInt(5) - 2;
            dirY = (int) ((random.nextInt(1) - 0.5) * (dirX == 0 ? 4 : 2));
        }

        for (int i = 0; i < walls.length; i++) {
            float coefficient = (float) (i) / (walls.length - 1);
            short ray = (short) (RAY_WALL_MIN + random.nextInt(RAY_WALL_MAX - RAY_WALL_MIN));
            short x = (short) ((origX + (isXShape ? shape.getComputedValue(coefficient, AMPLITUDE) : coefficient * AMPLITUDE) * dirX) % (WINDOW_WIDTH + RAY_WALL_MIN));
            short y = (short) ((origY + (isXShape ? coefficient * AMPLITUDE : shape.getComputedValue(coefficient, AMPLITUDE)) * dirY) % (WINDOW_HEIGHT + RAY_WALL_MIN));
            walls[i] = new Wall(x, y, ray);
        }
    }

    public Wall[] getWalls() {
        return walls;
    }

    public void dispose() {
        for (int i = 0; i < walls.length; i++)
            walls[i] = null;
    }
}
