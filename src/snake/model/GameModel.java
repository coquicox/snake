package snake.model;

import snake.control.ControlTimer;
import snake.model.exception.NotAPlayableSnake;
import snake.model.subgamemodel.EndCause;
import snake.model.subgamemodel.GameConstants;
import snake.model.subgamemodel.Snake;
import snake.model.subgamemodel.WallLine;
import snake.model.subgamemodel.solids.Fruits;
import snake.model.subgamemodel.solids.Wall;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by gcaron on 13/04/15.
 */
public class GameModel implements GameConstants, GlobalConstants {

    public final ArrayList<Fruits> fruits;
    public final WallLine[] wallLines;
    public final Random random;
    public final Snake[] players;
    private boolean isPaused;
    private boolean isEnd;
    private boolean isLaunched;
    private Timer timer;

    public GameModel(int nJoueur) {
        random = new Random();
        players = new Snake[nJoueur];
        fruits = new ArrayList<Fruits>(3);
        wallLines = new WallLine[N_WALL_LINE_MIN + random.nextInt(N_WALL_LINE_MAX - N_WALL_LINE_MIN)];
        defineWalls();
        defineSnakes();
        summonFruits();
        isPaused = false;
        isLaunched = false;
        isEnd=false;
    }

    public void setTimer(ControlTimer controlTimer) {
        timer = new Timer(16, controlTimer);
    }

    public void setTimerState() {
        if (isLaunched)
            timer.start();
        else
            timer.stop();
    }

    public boolean isLaunched() {
        return isLaunched;
    }

    public void setLaunched(boolean isLaunched) {
        this.isLaunched = isLaunched;
    }

    public void update() {
        if (!isPaused) {
            // Collision des joueurs
            for (Snake player : players) {
                player.update();
                for (Snake otherPlayer : players)
                    if (player.collideWith(otherPlayer))
                        partyEnd(player, (player.equals(otherPlayer)) ? EndCause.SELF_EATING : EndCause.EAT_THE_ENEMIES);
                for (WallLine wallLine : wallLines)
                    for (Wall wall : wallLine.getWalls())
                        if (player.collideWith(wall))
                            partyEnd(player, EndCause.SMASH_A_WALL);
                for (Fruits fruit : fruits)
                    player.collideWith(fruit);
            }

            // Peremption des fruits et création de nouveaux fruits si absence ou suppression de fruit
            ArrayList<Fruits> rmFruits = new ArrayList<Fruits>(1);
            for (Fruits fruit : fruits) {
                fruit.update();
                if (!fruit.isEatable())
                    rmFruits.add(fruit);
            }
            if (rmFruits.size() > 0 || fruits.size() == 0)
                summonFruits();
            for (Fruits fruit : rmFruits)
                fruits.remove(fruit);
        }
    }

    public void summonFruits() {
        int x = 0, y = 0;
        boolean regen = true;
        while (regen) {
            x = random.nextInt(WINDOW_WIDTH);
            y = random.nextInt(WINDOW_HEIGHT);
            regen = false;
            for (Fruits fruit : fruits)
                if (fruit.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_FRUITS))
                    regen = true;
            for (WallLine wallLine : wallLines)
                for (Wall wall : wallLine.getWalls())
                    if (wall.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_WALL))
                        regen = true;
            for (Snake player : players)
                if (player.head.collideWith(x, y, FRUITS_GENERATION_MARGIN_FOR_HEAD))
                    regen = true;
        }
        byte calories = (byte) (MIN_CALORIES + random.nextInt(MAX_CALORIES - MIN_CALORIES));
        short duration = (short) (DURATION_MAX - (calories - MIN_CALORIES) * DURATION_FACTOR);
        this.fruits.add(new Fruits((short) x, (short) y, calories, duration));
    }

    public void defineWalls() {
        for (int i = 0; i < wallLines.length; i++)
            wallLines[i] = new WallLine(random);
    }

    public void defineSnakes() {
        for (int i = 0; i < players.length; i++) {
            System.out.println("gamemodel : snake " + i);
            int x = SNAKE_GENERATION_MARGIN, y = SNAKE_GENERATION_MARGIN;
            boolean regen = true;
            while (regen) {
                x = SNAKE_GENERATION_MARGIN + random.nextInt(WINDOW_WIDTH - SNAKE_GENERATION_MARGIN * 2);
                y = SNAKE_GENERATION_MARGIN + random.nextInt(WINDOW_HEIGHT - SNAKE_GENERATION_MARGIN * 2);
                regen = false;
                for (WallLine wallLine : wallLines)
                    for (Wall wall : wallLine.getWalls())
                        if (wall.collideWith(x, y, SNAKE_GENERATION_MARGIN_FOR_OTHER))
                            regen = true;
                for (int j = 0; j < i; j++)
                    if (players[j].head.collideWith(x, y, SNAKE_GENERATION_MARGIN_FOR_OTHER))
                        regen = true;
            }
            //ToDo : Orienter la tête dès le départ pour éviter de foncer dans un mur
            players[i] = new Snake((short) x, (short) y);
        }
    }

    public void partyEnd(Snake looser, EndCause cause) {
        System.out.println(cause.makeEndSentences(looser));
        isPaused = true;
        isEnd =true;

        //ToDo : Gestion de la fin de partie avec looser le joueur perdant, et la cause de la défaite cause.
    }

    public Snake getPlayers(int n) throws NotAPlayableSnake {
        if (0 > n && n >= players.length)
            throw new NotAPlayableSnake(n);
        return players[n];
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }
    public boolean isEnd(){return isEnd;}

    public Snake[] getSnake() {
        return players;
    }

    //ToDo : Utiliser la méthode dispose() lors de la suppression du model actuel.
    public void dispose() {
        timer.stop();
        this.timer = null;
        int i = 0;
        for (i = 0; i < wallLines.length; i++) {
            wallLines[i].dispose();
            wallLines[i] = null;
        }
        fruits.clear();
        for (i = 0; i < players.length; i++) {
            players[i].dispose();
            players[i] = null;
        }
        isPaused = isLaunched = true;
    }

    public void renitialise() {

    }
}
