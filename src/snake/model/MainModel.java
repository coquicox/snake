package snake.model;

/**
 * Created by gcaron on 13/05/15.
 */
public class MainModel {
    private GameModel game;
    private MenuModel menu;

    public MainModel() {
        this.menu = new MenuModel();
        this.game = new GameModel(1);
    }
    public void newGame(){
        this.game=new GameModel(1);
    }
    public GameModel getGame() {
        return game;
    }

    public MenuModel getMenu() {
        return menu;
    }
}
