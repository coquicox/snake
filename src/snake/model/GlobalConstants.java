package snake.model;

/**
 * Created by chlorodatafile on 25/05/15.
 */
public interface GlobalConstants {
    public static final byte HEAD_RAY = 15;
    public static final byte TAIL_RAY = 13;
    public static final byte FRUITS_RAY = 15;

    public static final short WINDOW_WIDTH = 16 * 63;
    public static final short WINDOW_HEIGHT = 9 * 63;
}
