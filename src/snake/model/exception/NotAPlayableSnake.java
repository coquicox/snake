package snake.model.exception;

/**
 * Created by gcaron on 13/05/15.
 */
public class NotAPlayableSnake extends Exception {
    public NotAPlayableSnake(int n) {
        super("Snake number " + n + " is not a playable snake.");
    }
}
