package snake.control;

import snake.model.MainModel;
import snake.view.MainWindow;

/**
 * Created by gcaron on 13/05/15.
 */
public abstract class MainControl {
    protected static MainWindow actualWindow;
    protected static MainModel actualModel;

    public static void initControl(MainModel model) {
        actualModel = model;
        actualWindow = new MainWindow(model);
        actualModel.getGame().setTimer(new ControlTimer());
        new ControlMenu();
        new ControlGame();
        new ControlPause();
        new ControlLoose();
    }
}
