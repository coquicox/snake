package snake.control;

import snake.model.GameModel;
import snake.view.panels.PanelLoose;

import java.awt.*;
import java.awt.event.*;

/**
 * Created by Victor on 5/25/2015.
 */
public class ControlLoose extends MainControl implements MouseMotionListener, ActionListener, MouseListener {
    PanelLoose loose;

    public ControlLoose(){
        loose = actualWindow.getLoosePanel();
        loose.setMouseListener(this);
        loose.setActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==loose.newgame){
            actualWindow.setContentPane(actualWindow.getGamePanel());
            actualWindow.getGamePanel().setVisible(true);
            actualWindow.getGamePanel().requestFocus(true);
            actualWindow.getLoosePanel().setVisible(false);
            actualWindow.pack();
        }
        else {
            actualWindow.setContentPane(actualWindow.getMenuPanel());
            actualWindow.getMenuPanel().setVisible(true);
            actualWindow.getLoosePanel().setVisible(false);
            actualWindow.pack();
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource()==loose.newgame){
          loose.colornewgame=new Color(250,20,20);
        }
        else {
            loose.colorexit=new Color(250,20,20);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource()==loose.newgame){
            loose.colornewgame=new Color(0,0,0);
        }
        else {

            loose.colorexit=new Color(0,0,0);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
