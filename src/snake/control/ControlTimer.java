package snake.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlTimer extends MainControl implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        actualModel.getGame().update();
        actualWindow.getGamePanel().repaint();
    }
}
