package snake.control;

import snake.model.exception.NotAPlayableSnake;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ControlGame extends MainControl implements KeyListener {

    public ControlGame() {
        actualWindow.getGamePanel().setKeyControler(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!actualModel.getGame().isLaunched()) {
            actualModel.getGame().setLaunched(true);
            actualModel.getGame().setTimerState();
        }
        int keyCode = e.getKeyCode();
        if (!actualModel.getGame().isPaused()) {
            try {
                switch (keyCode) {
                    case KeyEvent.VK_UP:        //Fleche du haut
                        actualModel.getGame().getPlayers(0).accelerate(true);
                        break;
                    case KeyEvent.VK_LEFT:      //Fleche de gauche
                        actualModel.getGame().getPlayers(0).rotate('l');
                        break;
                    case KeyEvent.VK_RIGHT:    //Fleche de droite
                        actualModel.getGame().getPlayers(0).rotate('r');
                        break;
                    case KeyEvent.VK_ESCAPE:   //Bouton "echap"
                        actualModel.getGame().setPaused(!actualModel.getGame().
                                isPaused());
                        pause();
                        //actualWindow.pause();
                        //ToDo : Terminer fonction pause (interface entre autre)
                        break;
                }
                if (actualModel.getGame().players.length > 1) {
                    switch (keyCode) {
                        case KeyEvent.VK_Z:        //Fleche du haut
                            actualModel.getGame().getPlayers(1).accelerate(true);
                            break;
                        case KeyEvent.VK_Q:      //Fleche de gauche
                            actualModel.getGame().getPlayers(1).rotate('l');
                            break;
                        case KeyEvent.VK_D:    //Fleche de droite
                            actualModel.getGame().getPlayers(1).rotate('r');
                            break;
                    }
                }
            } catch (NotAPlayableSnake exception) {
                exception.printStackTrace();
            }
        }
        if (actualModel.getGame().isEnd()) {
            actualWindow.setContentPane(actualWindow.getLoosePanel());
            actualWindow.getLoosePanel().setVisible(true);
            actualWindow.getGamePanel().requestFocus(false);
            actualWindow.getGamePanel().setVisible(false);
            actualWindow.pack();
        }
    }

    private void pause() {
        /**
         * Pourquoi ? On a pas besoin d'actualiser le jeu durant la pause
         */
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        try {
            switch (keyCode) {
                case KeyEvent.VK_UP:        //Fleche du haut
                    actualModel.getGame().getPlayers(0).accelerate(false);
                    break;
                case KeyEvent.VK_LEFT:      //Fleche de gauche
                case KeyEvent.VK_RIGHT:    //Fleche de droite
                    actualModel.getGame().getPlayers(0).rotate('n');
                    break;
            }
            if (actualModel.getGame().players.length > 1) {
                switch (keyCode) {
                    case KeyEvent.VK_Z:        //Fleche du haut
                        actualModel.getGame().getPlayers(1).accelerate(false);
                        break;
                    case KeyEvent.VK_Q:      //Fleche de gauche
                    case KeyEvent.VK_D:    //Fleche de droite
                        actualModel.getGame().getPlayers(1).rotate('n');
                        break;
                }
            }
        } catch (NotAPlayableSnake exception) {
            exception.printStackTrace();
        }

    }
}
