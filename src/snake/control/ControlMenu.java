package snake.control;

import snake.view.panels.PanelMenu;

import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Mahel on 28/04/2015.
 */
public class ControlMenu extends MainControl implements MouseMotionListener, ActionListener, MouseListener {

    public TimerTask task;
    public Timer timer;
    PanelMenu menu;

    public ControlMenu() {
        menu = actualWindow.getMenuPanel();
        menu.setMenuControler(this);
        menu.setActionControler(this);
        menu.setMenuMouseControler(this);
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menu.playButton) {
            // menu.model.getGame().getSnake();
            actualWindow.setContentPane(actualWindow.getGamePanel());
            actualWindow.getGamePanel().setVisible(true);
            actualWindow.getGamePanel().requestFocus(true);
            actualWindow.getMenuPanel().setVisible(false);
            actualWindow.pack();

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        task = new TimerTask() {
            @Override
            public void run() {
                if (menu.step < 25) {
                    menu.step++;
                }
                menu.repaint();
                if (menu.step == 25) {
                    this.cancel();
                    timer.cancel();
                }
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 1);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        task = new TimerTask() {
            @Override
            public void run() {
                if (menu.step > 0) {
                    menu.step--;
                }
                if (menu.step == 0) {
                    this.cancel();
                    timer.cancel();
                }
                menu.repaint();
            }
        };
        timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 1);
    }
}
